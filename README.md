# Agile Team Poker

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.0.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## About

This frontend is part of the Poker Retrô project. This is a game that was created by [Dionatan Moura](https://br.linkedin.com/in/dionatan), and [Daniel Maria](https://br.linkedin.com/in/danielmariadasilva) developed this online version. You can read more about reading [this post](https://www.dbccompany.com.br/dbc/2020/06/30/agile-team-poker-dbc-um-assessment-para-times-ageis/)
