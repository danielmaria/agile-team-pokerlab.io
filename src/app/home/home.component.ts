import { Component, Output, EventEmitter, OnInit  } from '@angular/core';
import { environment } from './../../environments/environment';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  @Output() showHomeInfos: EventEmitter<String> =  new EventEmitter();
  constructor() {
    console.log(environment.production ? 'Ambiente de produção' : 'Ambiente de desenvolvimento')
   }

  ngOnInit(): void {
  }

  public changeToRoomComponent() {
    this.showHomeInfos.emit('room');
  }

  public changeToPlayComponent() {
    this.showHomeInfos.emit('play');
  }
}
