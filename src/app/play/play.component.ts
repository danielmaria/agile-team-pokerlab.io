import { Component, OnInit } from '@angular/core';
import { PlayerService } from '../services/player.service';
import { RoomService } from '../services/room.service';
import { Player } from '../models/player';
import { MatSnackBar } from '@angular/material/snack-bar';
import { OutRoom } from '../models/out-room';
import { InAnswer } from '../models/in-answer';
import { OutPlayer } from '../models/out-player';
import { OutAnswer } from '../models/out-answer';
import {TooltipPosition} from '@angular/material/tooltip';
import { NgxSpinnerService } from "ngx-spinner";  
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-play',
  templateUrl: './play.component.html',
  styleUrls: ['./play.component.css']
})
export class PlayComponent implements OnInit {
  player = {} as Player;
  outPlayer = {} as OutPlayer;
  returnRoom = {} as OutRoom;
  showPlayerScreen: boolean;
  activedStep = 0;
  question = {} as InAnswer;

  //TODO!
  cardEmotionHappy: string;
  cardEmotionOk: string;
  cardEmotionBad: string;
  cardIsGoingUp: string;
  cardIsGoingMedium: string;
  cardIsGoingDown: string;

  constructor(private playService: PlayerService, private roomService: RoomService, private activatedRoute: ActivatedRoute,
              private _snackBar: MatSnackBar, private SpinnerService: NgxSpinnerService) { }

  ngOnInit() {
    this.showPlayerScreen = false;
    this.cleanSelectionButtons();
    this.setRoomIfPassed();
  }

  private setRoomIfPassed() {
    this.activatedRoute.queryParams.subscribe((params) => {
      let orderObj = { ...params.keys, ...params };
      if (orderObj.room != undefined) {
        this.player.room = orderObj.room;
      }
    });
  }

  cleanSelectionButtons() {
    this.cardEmotionHappy = '';
    this.cardEmotionBad = '';
    this.cardEmotionOk = '';
    this.cardIsGoingUp = '';
    this.cardIsGoingMedium = '';
    this.cardIsGoingDown = '';
    this.question = {} as InAnswer;
  }
  
  createPlayer() {
    this.SpinnerService.show();
    this.playService.createPlayer(this.player).subscribe((outPlayer: OutPlayer) => {
      this.outPlayer = outPlayer;
      this.roomService.getRooms(this.player.room, this.player.name).subscribe((returnRoom: OutRoom) => {
        this.SpinnerService.hide();
        if(returnRoom.finishDate != null){
          this.openSnackBar('Esta retrospectiva já foi finalizada.');
          return;
        }
        this.returnRoom = returnRoom;
        this.makeMediatorScreen(returnRoom);
        this.openSnackBar('Sucesso');
      });
    }, error => {
      if (error.status == '404') {
        this.openSnackBar('Esta sala ainda não foi criado.');
      } else if (error.status == '403') {
        this.openSnackBar('Este jogador não pode ser incluído nesta sala.');
      }
      this.SpinnerService.hide(); 
    });
  }

  makeMediatorScreen(returnRoom: OutRoom) {
    this.returnRoom = returnRoom;
    this.showPlayerScreen = true;
  }

  selectCardEmotion(emotion: String, codeQuestion: number) {
    this.cardEmotionHappy = emotion == 'HAPPY' ? 'card-selected' : '';
    this.cardEmotionOk = emotion == 'OK' ? 'card-selected' : '';
    this.cardEmotionBad = emotion == 'BAD' ? 'card-selected' : '';
    this.question.question = codeQuestion;
    this.question.emotion = emotion;
    this.selectDefaultInformationsWhenSelectCard();
    this.sendChooseIfCardsSelected();
  }

  selectCardIsGoing(isGoing: String, codeQuestion: number) {
    this.cardIsGoingUp = isGoing == 'UP' ? 'card-selected' : '';
    this.cardIsGoingMedium = isGoing == 'MEDIUM' ? 'card-selected' : '';
    this.cardIsGoingDown = isGoing == 'DOWN' ? 'card-selected' : '';
    this.question.question = codeQuestion;
    this.question.isGoing = isGoing;
    this.selectDefaultInformationsWhenSelectCard();
    this.sendChooseIfCardsSelected();
  }

  private selectDefaultInformationsWhenSelectCard() {
    this.question.room = this.returnRoom.id;
    this.question.player = this.outPlayer.id;
  }
  private sendChooseIfCardsSelected() {
    if(this.question.isGoing != null && this.question.emotion != null) {
      this.addAnswer();
    }
  }

  addAnswer() {
    if (this.question.isGoing == null || this.question.emotion == null) {
      this.openSnackBar('Selecione uma carta de cada antes de enviar sua resposta.');
    } else {
      this.roomService.createAnswer(this.question).subscribe(() => {
        this.openSnackBar('Resposta enviada com sucesso.');
        let outAnswer = {} as OutAnswer;
        outAnswer.emotion = this.question.emotion;
        outAnswer.isGoing = this.question.isGoing;
        let answeres = this.returnRoom.questions.find(i => i.code == this.question.question).answers;
        if (answeres) {
          answeres.splice(0, 0, outAnswer);
        } else {
          this.returnRoom.questions.find(i => i.code == this.question.question).answers = [] as OutAnswer[];
          this.returnRoom.questions.find(i => i.code == this.question.question).answers.splice(0, 0, outAnswer);
        }
        this.cleanSelectionButtons();
      }, error => {
        if (error.status == '404') {
          this.openSnackBar('As informações solicitadas não encontraram nenhum usuario ou pergunta.');
        } else if (error.status == '401') {
          this.openSnackBar('Você não pode responder esta pergunta.');
          this.cleanSelectionButtons();
        } else if (error.status == '403') {
          this.openSnackBar('Esta pergunta não pode ser respondida ainda.');
        }
      });
    }
  }

  prevStep(step) {
    this.activedStep = step - 1;
    this.cleanSelectionButtons();
  }

  nextStep(step) {
    this.activedStep = step + 1;
    this.cleanSelectionButtons();
  }

  stepIsValid() {
    return this.activedStep < this.returnRoom.questions.length;
  }

  openSnackBar(mensagem: string) {
    this._snackBar.open(mensagem, 'Fechar', {
      duration: 2500,
      horizontalPosition: 'end',
      verticalPosition: 'top',
    });
  }
}
