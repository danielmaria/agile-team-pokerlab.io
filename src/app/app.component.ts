import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'Agile Team Poker';
  public showHomeInfos: boolean = true;
  public showPlayScreen: boolean = false;
  public showRoomScreen: boolean = false;

  constructor(private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((params) => {
      let orderObj = { ...params.keys, ...params };
      if(orderObj.room != undefined){
        this.changedHomeInfos('play');
      }
    });
  }
  
  changedHomeInfos(showWhat: String) {
    this.showHomeInfos = false;
    if(showWhat == 'play'){
      this.showPlayScreen = true;
    } else {
      this.showRoomScreen = true;
    }
  }

}
