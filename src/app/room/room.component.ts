import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Room } from '../models/room';
import { RoomService } from '../services/room.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import {TooltipPosition} from '@angular/material/tooltip';
import { OutRoom } from '../models/out-room';
import { OutAnswer } from '../models/out-answer';
import { NgxSpinnerService } from "ngx-spinner";
import {formatDate} from '@angular/common';

@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.css']
})
export class RoomComponent implements OnInit {
  room = {} as Room;
  returnRoom = {} as OutRoom;
  showMediatorScreen : boolean;
  showReport : boolean;
  consensusSelected = 0;
  
  constructor(private roomService: RoomService, private _snackBar: MatSnackBar, private SpinnerService: NgxSpinnerService) { }

  ngOnInit(): void {
    this.showMediatorScreen = false;
    this.showReport = false;
  }

  createRoom(){
    this.SpinnerService.show();
    this.roomService.createRooms(this.room).subscribe((roomRetorn: OutRoom) => {
      this.makeMediatorScreen(roomRetorn);
      this.openSnackBar('Sucesso');
      this.SpinnerService.hide();
      this.countConsensus();

    }, error => {
      if(error.status == 400) {
        this.openSnackBar('Você precisa informar o nome da sala e o nome do scrum master.');
      } else if(error.status == 403){
        this.openSnackBar('Esta sala já foi criada e não pertence a este ScrumMaster.');
      }
      this.SpinnerService.hide();
    });
  }

  copyLinkRoom() {
    let url = window.location.href + '?room=' + this.room.name;
    let listener = (e: ClipboardEvent) => {
      e.clipboardData.setData('text/plain', (url));
      e.preventDefault();
  };

  document.addEventListener('copy', listener);
  document.execCommand('copy');
  document.removeEventListener('copy', listener);
  this.openSnackBar('O link com o nome da sala foi copiado.');
  }

  makeMediatorScreen(returnRoom: OutRoom) {
    this.returnRoom = returnRoom;
    this.showMediatorScreen = true;
  }

  releaseThisQuestion(code : number){
    this.roomService.releaseQuestion(this.returnRoom.name, code, this.returnRoom.scrumMaster).subscribe(() => {
      this.returnRoom.questions.find(i => i.code == code).release = true;
    }, error => {
      if(error.status == 403){
        this.openSnackBar('Você não tem permissão para isso.');
      }
    });
  }

  saveRoom(){
    this.roomService.finishRetrospective(this.returnRoom).subscribe((newRoom: OutRoom) => {
      this.returnRoom = newRoom;
    } , error => {
      if (error.status == 500) {
        this.openSnackBar('O serviço está fora. Tente novamente mais tarde.');
      }
    });
  }

  finishRetrospective() {
    if (confirm("Você tem certeza que deseja encerrar esta retrospectiva? Após isso todas as perguntas serão bloqueadas para os jogadores. " + this.formatMessageConsensusLeft())) {
      this.returnRoom.finishDate = formatDate(new Date(), 'yyyy-MM-dd', 'en-US');
      this.roomService.finishRetrospective(this.returnRoom).subscribe((newRoom: OutRoom) => {
        this.returnRoom = newRoom;
      }, error => {
        if (error.status == 500) {
          this.openSnackBar('O serviço está fora. Tente novamente mais tarde.');
        }
      });
    }
  }

  private formatMessageConsensusLeft() {
    if(this.consensusSelected > 0) {
      return "Lembre-se que faltam " + this.consensusSelected + " consensos do time que não foram selecionados ainda.";
    }
    return '';
  }

  selectConsensus(answer : OutAnswer, codeQuestion: number){
    this.returnRoom.questions.find(i => i.code == codeQuestion).answers.forEach(a => a.consensus = false);
    answer.consensus = true;
    this.countConsensus();
  }

  private countConsensus(){
    let countConsensus = 0;
    this.returnRoom.questions.forEach(q => {
      if(!this.hasConsensus(q.answers)){
        countConsensus++;
      }
    });
    this.consensusSelected = countConsensus;
  }

  private hasConsensus(answers: OutAnswer[]){
    if(answers == null) return false;
    let has = false;
    answers.forEach(a => {
      if(a.consensus) {
        has = true;
      }
    });
    return has;
  }

  refreshRoom(){
    this.roomService.createRooms(this.room).subscribe((roomRetorn: OutRoom) => {
      this.returnRoom = roomRetorn;
    });

  }

  generateReport(){
    this.showReport = true;
    if(this.returnRoom.finishDate == null){
      this.refreshRoom();
    }
  }

  openSnackBar(mensagem: string) {
    this._snackBar.open(mensagem, 'Fechar', {
      duration: 2500,
      horizontalPosition: 'end',
      verticalPosition: 'top',
    });
  }

}
