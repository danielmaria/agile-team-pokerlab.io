import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Room } from '../models/room';
import { OutRoom } from '../models/out-room';
import { InAnswer } from '../models/in-answer';
import { environment } from './../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RoomService {
  url =  environment.apiUrl + 'v1/rooms';

  constructor(private httpClient: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  }

  getRooms(name: String, player : String): Observable<OutRoom> {
    return this.httpClient.get<OutRoom>(this.url + '/' + name + '?player=' + player, this.httpOptions);
  }

  createRooms(room: Room): Observable<OutRoom> {
        return this.httpClient.post<OutRoom>(this.url, JSON.stringify(room), this.httpOptions);
  }

  releaseQuestion(roomName: String, questionCode : number, scrumMasterName : String): Observable<void> {
    return this.httpClient.patch<void>(this.url, JSON.stringify({ 'room': roomName, 'scrumMaster': scrumMasterName, 'code': questionCode}), this.httpOptions)
      .pipe(
        retry(2)
      )
  }

  finishRetrospective(room: OutRoom): Observable<OutRoom> {
    return this.httpClient.put<OutRoom>(this.url, JSON.stringify(room), this.httpOptions);
  }

  createAnswer(answer: InAnswer): Observable<void> {
      return this.httpClient.put<void>(this.url + '/' + answer.room + '/question/' + answer.question, JSON.stringify(answer), this.httpOptions)
  }
  
  // Manipulação de erros
  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Erro ocorreu no lado do client
      errorMessage = error.error.message;
    } else {
      // Erro ocorreu no lado do servidor
      errorMessage = `Código do erro: ${error.status}, ` + `menssagem: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  };
}
