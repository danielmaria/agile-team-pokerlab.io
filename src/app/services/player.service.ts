import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Player } from '../models/player';
import { OutPlayer } from '../models/out-player';
import { environment } from './../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PlayerService {
  url = environment.apiUrl + 'v1/players';

  constructor(private httpClient: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  }

  createPlayer(player: Player): Observable<OutPlayer> {
      return this.httpClient.post<OutPlayer>(this.url, JSON.stringify(player), this.httpOptions);
  }

  // Manipulação de erros
  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Erro ocorreu no lado do client
      errorMessage = error.error.message;
    } else {
      // Erro ocorreu no lado do servidor
      errorMessage = `Código do erro: ${error.status}, ` + `menssagem: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  };
}
