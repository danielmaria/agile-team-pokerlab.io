import { Component, OnInit, Input } from '@angular/core';
import { OutQuestion } from '../models/out-question';
import { OutAnswer } from '../models/out-answer';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {

  @Input() roomInformations;
  consensusEmotion = new Map();
  consensusIsGoing = new Map();
  atentionPoint = new Map();

  constructor() { 
  }

  ngOnInit(): void {
    this.buildReport();
  }

  buildReport() {
    this.roomInformations.questions.forEach(q => {
      if(q.answers){
        q.answers.forEach(a => {
          if(a != undefined && a.consensus) {
            this.consensusEmotion.set(q.code, a.emotion);
            this.consensusIsGoing.set(q.code, a.isGoing);
            if(this.verifyCaution(a)) {
              this.atentionPoint.set(q.code, true)
            }
          }
        });
      }
    });
  }
  verifyCaution(question: OutAnswer){
    return (question.isGoing === 'DOWN' || question.emotion === 'BAD' || (question.isGoing === 'MEDIUM' && question.emotion === 'OK'));
  }

  findConsensusEmotion(question: OutQuestion) {
    if(question.answers) {
      let answerd = question.answers.find(a => a.consensus == true);

      if(answerd) {
        return answerd.emotion;
      }
    }
  }

  findConsensusIsGoing(question: OutQuestion) {
    if(question.answers) {
      let answerd = question.answers.find(a => a.consensus == true);

      if(answerd) {
        return answerd.isGoing;
      }
    }
  }

}
