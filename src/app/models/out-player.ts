export interface OutPlayer {
    name: String;
    room: String;
    id: String;
}
