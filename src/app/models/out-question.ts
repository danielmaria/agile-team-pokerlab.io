import { OutAnswer } from './out-answer';


export interface OutQuestion {
    code : number;
    name : String;
    release : boolean;
    answers : OutAnswer[]
}
