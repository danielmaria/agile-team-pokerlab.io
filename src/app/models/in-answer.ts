export interface InAnswer {
    player : String;
    question : number;
    emotion : String;
    isGoing : String;
    room : String;
}