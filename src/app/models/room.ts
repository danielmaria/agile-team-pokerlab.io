export interface Room {
    id: String;
    name: String;
    players: String;
    scrumMaster : String;
}
