export interface OutAnswer {
    player : String;
    emotion : String;
    isGoing : String;
    consensus : boolean;
}
