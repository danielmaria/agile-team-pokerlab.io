import { OutQuestion } from './out-question';

export interface OutRoom {
    id : String;
    name : String;
    scrumMaster : String;
    questions : OutQuestion[];
    finishDate: String;
}
