import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoomComponent } from './room/room.component';
import { PlayComponent } from './play/play.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: 'room-component', component: RoomComponent },
  { path: 'play-component', component: PlayComponent },
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
